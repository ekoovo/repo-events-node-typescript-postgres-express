# REPO EVENTS - NODE EXPRESS POSTGRES APP



This app uses the above named technologies to carry out operations of a repo event. Events and their actors on repos are modeled. 

To view a list of the operations possible visit this [link](https://github.com/ben-x/express-dataset)


To run the app:

- Clone the repo
- Make sure you have node and postgres installed and configured

- Create a database and get the connection string

- Run ``` npm install ``` to install the require node modules

- Run ``` npm run complie ``` to run a script that compile the typescript code to javascript

- Add a ```.env``` file with the postgres connection string as described in the ```.env.example``` file

- Run ``` npm run migrate up ``` to run the db migration and create the schema in the DB

- Run the program with ```npm run start```
