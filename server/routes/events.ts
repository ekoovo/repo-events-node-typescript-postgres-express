import express from "express";
import sql from "../db/sql";
import {
  postEvent,
  getEvents,
  getActorEvents,
} from "../controllers/eventsController";

const router = express.Router();

/* POST an event. */
router.post("/", async (req, res) => {
  try {
    const Event = await postEvent(req.body);
    res.status(201).json({ message: "created successfully", data: Event });
  } catch (error) {
    res.status(400).json({
      message: `failed to create. There was an error: ${error.message}`,
    });
  }
});

/* GET  events. */
router.get("/", async (req, res) => {
  try {
    const Events = await getEvents();
    res.status(200).json(Events);
  } catch (error) {
    res.status(400).json({
      message: `failed to get events. There was an error: ${error.message}`,
    });
  }
});

/* GET Actor's events. */
router.get("/actors/:actorID", async (req, res) => {
  try {
    const Events = await getActorEvents(req.params.actorID);
    res.status(200).json(Events);
  } catch (error) {
    res.status(404).json({
      message: `failed to get Actor's events. There was an error: ${error.message}`,
    });
  }
});

export default router;
