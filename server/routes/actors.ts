import express from "express";
import {
  updateActor,
  getActorsByEventTimestampLogin,
} from "../controllers/actorsController";

const router = express.Router();

/* PUT Actor's Avatar update. */
router.put("/", async function (req, res) {
  try {
    const response = await updateActor(req.body);
    res.status(200).end();
  } catch (error) {
    if (error.message === "Actor not found") {
      console.log(error.message);
      res.status(404).end();
    } else {
      res.status(400).json({
        message: `failed to update events. There was an error: ${error.message}`,
      });
    }
  }
});

/* GET users Actor record. */
router.get("/", async function (req, res) {
  try {
    const response = await getActorsByEventTimestampLogin();
    res.status(200).json(response);
  } catch (error) {
    res.status(400).json({
      message: `failed to update events. There was an error: ${error.message}`,
    });
  }
});

/* GET users Actor record by streak. */
router.get("/streak", async function (req, res) {
  try {
    const response = await getActorsByEventTimestampLogin();
    res.status(200).json(response);
  } catch (error) {
    res.status(400).json({
      message: `failed to update events. There was an error: ${error.message}`,
    });
  }
});

export default router;
