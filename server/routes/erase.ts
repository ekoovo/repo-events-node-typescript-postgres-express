import express from "express";
import eraseEvents from "../controllers/eraseController";

const router = express.Router();

/* Erase all events. */
router.delete("/", async function (req, res) {
  try {
    await eraseEvents();
    res.status(200).json({ message: "Erased successful" });
  } catch (error) {
    res.status(400).json({
      message: `failed to erase. There was an error: ${error.message}`,
    });
  }
});

export default router;
