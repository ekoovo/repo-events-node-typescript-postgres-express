import postgres from "postgres";

const sql = postgres(process.env.DATABASE_URL!);
console.log("database connected");
export default sql;
