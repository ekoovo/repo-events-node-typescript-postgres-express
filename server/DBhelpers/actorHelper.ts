import sql from "../db/sql";

export const getActorDetail = async (id: string | number) => {
  return sql`SELECT * FROM actors WHERE id = ${id}`;
};

export const updateAvatar = async (id: string | number, avatar: string) => {
  return sql`UPDATE actors SET avatar_url = ${avatar} WHERE id = ${id}`;
};

export const getActorsBy = () => {
  return sql`
        SELECT actors.id, actors.login, actors.avatar_url, events.created_at FROM actors
        JOIN events ON actors.id = events.actor_id GROUP BY actors.id, events.created_at
        ORDER BY COUNT(events.actor_id), DATE(events.created_at) DESC, actors.login DESC
    `;
};

export const structActors = (actor: Record<string, any>) => ({
  id: actor.id,
  login: actor.login,
  avatar_url: actor.avatar_url,
});
