import sql from "../db/sql";

export default async function eraseAll() {
  sql`
        DELETE FROM events
    `;
  sql`
        DELETE FROM actors
    `;
  sql`
        DELETE FROM repos
    `;
  return;
}
