import sql from "../db/sql";

export const getEventId = async (id: string | number) => {
  return sql`SELECT event_id FROM events WHERE event_id = ${id}`;
};

export const getActorId = async (id: string | number) => {
  return sql`SELECT id FROM actors WHERE id = ${id}`;
};

export const createActor = async (actor: Record<string, any>) => {
  return sql`
      INSERT INTO actors ${sql(actor, "id", "login", "avatar_url")}
    `;
};

export const createRepo = async (repo: Record<string, any>) => {
  return sql`
      INSERT INTO repos ${sql(repo, "id", "name", "url")}
    `;
};

export const createEvent = async (event: Record<string, any>) => {
  return sql`
      INSERT INTO events ${sql(
        event,
        "event_id",
        "type",
        "actor_id",
        "repo_id",
        "created_at"
      )}
    RETURNING *`;
};

export const retrieveEventsDetails = async (id?: string | number) => {
  if (id) {
    return sql`
      SELECT * FROM events
      JOIN actors ON events.actor_id = actors.id
      JOIN repos ON events.repo_id = repos.id WHERE actors.id = ${id} ORDER BY events.event_id 
    `;
  } else {
    return sql`
      SELECT * FROM events
      JOIN actors ON events.actor_id = actors.id
      JOIN repos ON events.repo_id = repos.id  ORDER BY events.event_id 
    `;
  }
};

export const restructEvent = (event: Record<string, unknown>) => ({
  id: event.event_id,
  type: event.type,
  actor: {
    id: event.actor_id,
    login: event.login,
    avatar_url: event.avatar_url,
  },
  repo: {
    id: event.repo_id,
    name: event.name,
    url: event.url,
  },
  created_at: event.created_at,
});
