import eraseAll from "../DBhelpers/eraseHelper";

const eraseEvents = async () => eraseAll();
export default eraseEvents;
