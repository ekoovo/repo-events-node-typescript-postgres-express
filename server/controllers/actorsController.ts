import {
  getActorDetail,
  updateAvatar,
  getActorsBy,
  structActors,
} from "../DBhelpers/actorHelper";

export const updateActor = async (body: Record<string, any>) => {
  if (
    Object.keys(body).length > 3 ||
    !body.id ||
    !body.login ||
    !body.avatar_url
  )
    throw Error("wrong operation");
  const actorDetails = await getActorDetail(body.id);
  if (!actorDetails[0]) throw Error("Actor not found");
  if (actorDetails[0].login !== body.login) throw Error("wrong operation");

  return await updateAvatar(body.id, body.avatar_url);
};

export const getActorsByEventTimestampLogin = async () => {
  const actors = await getActorsBy();
  const actorsMod = actors.map(structActors);
  return actorsMod;
};
