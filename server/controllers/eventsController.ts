import {
  getEventId,
  getActorId,
  createActor,
  createRepo,
  createEvent,
  retrieveEventsDetails,
  restructEvent,
} from "../DBhelpers/eventHelper";

export const postEvent = async (body: Record<string, any>) => {
  const { id, type, actor, repo, created_at } = body;
  const DB_id = await getEventId(id);

  if (DB_id.count) throw Error("Id already exist");

  await createActor(actor);
  await createRepo(repo);
  const event = {
    event_id: id,
    type,
    actor_id: actor.id,
    repo_id: repo.id,
    created_at,
  };
  const newEvent = await createEvent(event);
  return newEvent;
};

export const getEvents = async () => {
  const retrievedEvents = await retrieveEventsDetails();
  return retrievedEvents.map(restructEvent);
};

export const getActorEvents = async (id: string | number) => {
  const actorID = await getActorId(id);
  if (!actorID[0]) throw Error("Actor not found");

  const retrievedEvents = await retrieveEventsDetails(actorID[0].id);
  return retrievedEvents.map(restructEvent);
};
