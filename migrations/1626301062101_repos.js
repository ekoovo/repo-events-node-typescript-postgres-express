/* eslint-disable camelcase */

// exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable("repos", {
    id: {
      type: "bigint",
      notNull: true,
      primaryKey: true,
    },
    name: { type: "varchar(150)", notNull: true },
    url: { type: "varchar(150)", notNull: true },
  });
};

exports.down = (pgm) => {
  pgm.dropTable("repos");
};