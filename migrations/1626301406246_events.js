/* eslint-disable camelcase */

// exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable("events", {
    event_id: {
      type: "bigint",
      notNull: true,
      primaryKey: true,
    },
    type: { type: "varchar(150)", notNull: true },
    actor_id: {
      type: "bigint",
      notNull: true,
      references: 'actors',
      onDelete: "cascade",
    },
    repo_id: {
      type: "bigint",
      notNull: true,
      references: 'repos',
      onDelete: "cascade",
    },
    created_at: {
      type: "varchar(150)",
      notNull: true,
    },
  });
};

exports.down = (pgm) => {
  pgm.dropTable("events");
};
