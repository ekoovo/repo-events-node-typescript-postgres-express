/* eslint-disable camelcase */

// exports.shorthands = undefined;

exports.up = pgm => {
    pgm.createTable("actors", {
      id: {
        type: "bigint",
        notNull: true,
        primaryKey: true,
      },
      login: { type: "varchar(150)", notNull: true },
      avatar_url: { type: "varchar(150)", notNull: true },
    });
};

exports.down = pgm => {
    pgm.dropTable("actors");
};
